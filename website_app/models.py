
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_user_model
from django.urls import reverse


class User(AbstractUser):
    is_admin = models.BooleanField(default=False)
    is_reviewer = models.BooleanField(default=False)
    is_editor = models.BooleanField(default=False)
    is_author = models.BooleanField(default=False)
    is_poster = models.BooleanField(default=False)


    class Meta:
        swappable = 'AUTH_USER_MODEL'

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    TSOL_ZEREG = (
        ('pro', 'Профессор'),('ded pro', 'Дэд профессор' ),
        ( 'Dok', 'Доктор(Sc.D)'),('Dok ph', 'Доктор(Ph.D)'),
        ('mag', 'Магистр'), ('bak', 'Бакалавр'),
    )
    orcid = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=200, null=True)
    tsol_zereg = models.CharField(max_length=200, null=True, choices=TSOL_ZEREG)
    work = models.CharField(max_length=200, null=True, blank=True )
    profile_image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    created_at=models.DateTimeField(auto_now_add=True,  null=True)
    update_at=models.DateTimeField(auto_now_add=True,  null=True)
    def __str__(self):
        return self.user.username
        
    

class Jurnalcontent(models.Model):
    name = models.CharField(max_length=250, null=True)
    def __str__(self):
        return self.name

class Volume(models.Model):
    vol_title = models.TextField(null=True)
    vol_description = models.TextField(null=True)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    pdf = models.FileField(upload_to='volume/pdfs/',null=True)
    vol_status = models.CharField(max_length=200, null=True)
    vol_print = models.CharField(max_length=200, null=True)
    vol_price = models.CharField(max_length=200, null=True)
    created_at=models.DateTimeField(auto_now_add=True,  null=True)
    update_at=models.DateTimeField(auto_now_add=True,  null=True)

class Paper(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.ForeignKey(Jurnalcontent,on_delete=models.CASCADE)
    p_name=models.TextField(null=True)
    p_key=models.TextField(null=True)
    p_title=models.TextField(null=True)
    abstarct = models.TextField(null=True)
    word_file=models.FileField(upload_to='words',  null=True)
    pdf_file=models.FileField(upload_to='pdf',  null=True)
    created_at=models.DateTimeField(auto_now_add=True,  null=True)
    update_at=models.DateTimeField(auto_now_add=True,  null=True)

    def __str__(self):
        return self.p_name

class StatusEditor(models.Model):
    status_name = models.CharField(max_length=200, null=True)
 

class ReviewerStatus(models.Model):   
    status_review = models.CharField(max_length=200, null=True)

class Reviews(models.Model):
    reviewer = models.ForeignKey(User, on_delete=models.CASCADE)
    paper = models.ForeignKey(Paper, on_delete=models.CASCADE)
    review_status = models.ForeignKey(ReviewerStatus, on_delete=models.CASCADE)
    review_file = models.FileField(upload_to='review/pdf',  null=True)
    review_title = models.TextField(null=True)
    date =models.DateTimeField(auto_now_add=False,  null=True)
    created_at=models.DateTimeField(auto_now_add=True,  null=True)
    update_at=models.DateTimeField(auto_now_add=True,  null=True)

class ReviewerPaper(models.Model):
    paper = models.ForeignKey(Paper, on_delete=models.CASCADE)
    reviewer_id = models.ForeignKey(Reviews, on_delete=models.CASCADE)
    is_status = models.CharField(max_length=200, null=True)
    created_at=models.DateTimeField(auto_now_add=True,  null=True)
    update_at=models.DateTimeField(auto_now_add=True,  null=True)


class Webpost(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vol_title = models.TextField(null=True)
    content = models.TextField(null=True)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    pub_date = models.CharField(max_length=200, null=True)
    is_status = models.CharField(max_length=200, null=True)
    created_at=models.DateTimeField(auto_now_add=True,  null=True)
    update_at=models.DateTimeField(auto_now_add=True,  null=True)

class VolumeOrder(models.Model):
    volume_id =models.ForeignKey(Volume, on_delete=models.CASCADE)
    too = models.CharField(max_length=200, null=True)
    is_status = models.CharField(max_length=200, null=True)
    created_at=models.DateTimeField(auto_now_add=True,  null=True)
    update_at=models.DateTimeField(auto_now_add=True,  null=True)

