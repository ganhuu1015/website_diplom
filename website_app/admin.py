from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from website_app.models import User, Jurnalcontent
# Register your models here.
admin.site.register(User, UserAdmin)
admin.site.register(Jurnalcontent)