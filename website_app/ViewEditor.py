from django.shortcuts import render,redirect
from .models import Profile, ReviewerStatus,ReviewerStatus, Paper, Reviews,Jurnalcontent,Volume,VolumeOrder,Webpost,User
from django.contrib import messages
from django.contrib.auth.models import Group
from django.http import HttpResponse
from io import BytesIO
from django.template.loader import get_template
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, DetailView, DeleteView, UpdateView, ListView

def EditorDashboard(request):
    all_users_count=User.objects.all().count()
    all_review_count =User.objects.all().filter(is_reviewer = 1).count()
    all_paper_count = Paper.objects.all().count()


    context = {
        'all_users_count':all_users_count,
        'all_review_count':all_review_count,
        'all_paper_count':all_paper_count
    }
    
    return render(request, 'editor/dashboard.html',context)

def manager_author(request):
    authors = User.objects.all()
    context ={
        'authors':authors
    }
    return render(request, 'editor/manage_author.html', context)

def editor_profile(request):
    user = User.objects.get(id=request.user.id)
    return render(request, 'editor/editorProfile.html', {'user': user})

def editor_profile_update(request):
    if request.method != "POST":
        messages.error(request, "Invalid Method!")
        return redirect('editor_profile')
    else:
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('lsat_name')
        password = request.POST.get('password')

        try:
            customuser = User.objects.get(id=request.user.id)
            customuser.first_name = first_name
            customuser.last_name = last_name
            if password != None and password != "":
                customuser.set_password(password)
            customuser.save()
            messages.success(request, "Profile Updated Successfully")
            return redirect('editor_profile')
        except:
            messages.error(request, "Failed to Update Profile")
            return redirect('editor_profile')       

def send_to_review(request):
    return render(request, 'editor/send_to_review.html')

def manager_user(request):
    users = User.objects.all()
    context ={
        'users':users
    }
    return render(request, 'editor/manage_user.html', context)

def add_user(request):
    return render(request, 'editor/add_user.html')


def add_author_save(request):
    if request.method != "POST":
        messages.error(request, "Invalid Method ")
        return redirect('add_author')
    else:
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        try:
            user = User.objects.create_user(username=username, password=password, email=email, first_name=first_name, last_name=last_name)
            user.save()

            messages.success(request, "Staff Added Successfully!")
            return redirect('add_author')
        except:
            messages.error(request, "Failed to Add Staff!")
            return redirect('add_author')
  
def manager_review(request):
    reviews = User.objects.all().filter(is_reviewer = 1)
    pros = Profile.objects.all()
    context ={
        'reviews':reviews,
        'pors':pros
    }
    return render(request, 'editor/review/manage_review.html', context)

def manager_review_add(request):
    return render(request, 'editor/review/add_reviewer.html')


class ABookListView(LoginRequiredMixin,ListView):
	model = Paper
	template_name = 'editor/manage_article.html'
	context_object_name = 'papers'
	paginate_by = 8

	def get_queryset(self):
		return Paper.objects.order_by('-id')


