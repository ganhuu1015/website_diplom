from django.shortcuts import render
from django.shortcuts import redirect, render,HttpResponse,Http404
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views import generic
from bootstrap_modal_forms.mixins import PassRequestMixin
from .models import Profile, ReviewerStatus,ReviewerStatus, Paper, Reviews,Jurnalcontent,Volume,VolumeOrder,Webpost,User
from django.contrib import messages
from django.db.models import Sum
from django.views.generic import CreateView, DetailView, DeleteView, UpdateView, ListView
from .forms import CreateUserForm, UserForm
from . import models
import operator
import itertools
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate, logout
from django.contrib import auth, messages
from django.contrib.auth.hashers import make_password
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.contrib.auth.models import Group
from django.core.mail import send_mail
from django.conf import settings

def homePage(request):
    roles = User.objects.all()
    return render(request, 'main/home.html', {'roles':roles})

def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None and user.is_active:
            auth.login(request,user)
            if  user.is_admin or user.is_superuser:
                return redirect('admindashboard')
            if user.is_reviewer:
                return redirect('index')
            if user.is_editor:
                return redirect('editordashboard')
            else:
                return redirect('/')
        else:
            messages.info(request, 'Username OR password is incorrect')
    return render(request, 'users/login.html')

def register(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.is_author = 1
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='Author')
            user.groups.add(group)
            Profile.objects.create(
                user=user,
            )
            messages.success(request, 'Account was created for ' + username)
            return redirect('login')
    context = {'form':form}
    return render(request, 'users/register.html', context )


def logout_out(request):
    logout(request)
    return redirect("/")

def about(request):
    return render(request, 'main/about.html')

def editor(request):
    return render(request, 'main/editor.html')

def articleSubmit(request):
    return render(request, 'main/articleSubmit.html')
    
def huuliPage(request):
    return render(request, 'main/huuliPage.html')

def submit_index(request):
    return render(request, 'main/submitarticle.html')


# article send
def article_send(request):
    if request.method == 'POST':
        p_name = request.POST['p_name']
        abstarct = request.POST['abstarct']
        p_key = request.POST['p_key']
        p_title = request.POST['p_title']
        pdf_file = request.FILES['pdf_file']
        word_file = request.FILES['word_file']
        com = request.user
        user_id = com.id
        article1 = Paper(p_name=p_name, abstarct =abstarct, 
                           p_key=p_key, p_title=p_title,
                           pdf_file=pdf_file, word_file=word_file, user_id = user_id, status_id="1")
        article1.save()
        email_body = ' Шинэ өгүүлэл ирсэн '
        send_mail( 'ЛАВАЙ - Эрдэм шинжилгээний сэтгүүлийн вэб сайт',
        email_body,
         settings.EMAIL_HOST_USER,
         ['lavai.msue@edu.com'],
         fail_silently=False
        )
        messages.success(request, 'Өгүүлэл амжилттай илгээлээ')
        return redirect('index_Article')



def article_list(request):
    papers = request.user.paper_set.all()
    total_paper = papers.count()
    pending = papers.filter(status='1').count()
    Rejected = papers.filter(status='7').count()
    editor = papers.filter(status='2').count()
    editor1 = papers.filter(status='6').count()

    context = {
        'total_paper':total_paper,
        'pending':pending,
        'Rejected':Rejected,
        'editor':editor,
        'editor1':editor1
    }

    return render(request, 'main/aticle-list.html', context)
# admin views
# def edit_user(request, user_id):
#     user = User.objects.get(id=user_id)

#     context = {
#         "user": user,
#         "id": user_id
#     }
#     return render(request, "admin/edit_user_template.html", context)
class edit_user(SuccessMessageMixin, UpdateView): 
    model = User
    form_class = UserForm
    template_name = 'admin/edit_user_template.html'
    success_url = reverse_lazy('manage_user_index')
    success_message = "Data successfully updated"

# def edit_user_save(request):
#     if request.method != "POST":
#         return Http404("<h2>Method Not Allowed</h2>")
#     else:
#         user_id = request.POST.get('user_id')
#         username = request.POST.get('username')
#         email = request.POST.get('email')
#         first_name = request.POST.get('first_name')
#         last_name = request.POST.get('last_name')

#         try:
#             # INSERTING into Customuser Model
#             user = User.objects.get(id=user_id)
#             user.first_name = first_name
#             user.last_name = last_name
#             user.email = email
#             user.username = username
#             user.save()
#             messages.success(request, "Staff Updated Successfully.")
#             return redirect('/edit_user/'+user_id)

#         except:
#             messages.error(request, "Failed to Update Staff.")
#             return redirect('/edit_user/'+user_id)


def adminDashboard(request):
    all_users_count=User.objects.all().count()
    all_volume_count =Volume.objects.all().count()
    all_post_count = Webpost.objects.all().count()


    context = {
        'all_users_count':all_users_count,
        'all_volume_count':all_volume_count,
        'all_post_count':all_post_count
    }
    
    return render(request, 'admin/adminDashboard.html', context)

def add_user(request):
    choice = ['1', '0','Author', 'Editor','Reviewer']
    choice = {'choice': choice}
    return render(request, 'admin/add_user.html',choice)

def manage_user_index(request):
    all_users=User.objects.all()
    context = {
        'all_users':all_users
    }
    return render(request, 'admin/manage_user.html',context)

def create_user(request):
    choice = ['1', '0',  'Author', 'Editor','Reviewer']
    choice = {'choice': choice}
    if request.method == 'POST':
            first_name=request.POST['first_name']
            last_name=request.POST['last_name']
            username=request.POST['username']
            userType=request.POST['userType']
            email=request.POST['email']
            password=request.POST['password']
            password = make_password(password)
            print("User Type")
            print(userType)
            if userType == "Author":
                a = User(first_name=first_name, last_name=last_name, username=username, email=email, password=password, is_author=True)
                a.save()
                messages.success(request, 'Member was created successfully!')
                return redirect('add_user')
            elif userType == "Editor":
                a = User(first_name=first_name, last_name=last_name, username=username, email=email, password=password, is_editor=True)
                a.save()
                messages.success(request, 'Member was created successfully!')
                return redirect('add_user')
            elif userType == "Reviewer":
                a = User(first_name=first_name, last_name=last_name, username=username, email=email, password=password, is_reviewer=True)
                a.save()
                messages.success(request, 'Member was created successfully!')
                return redirect('add_user')
            else:
                messages.success(request, 'Member was not created')
                return redirect('add_user')
    else:
        return redirect('manage_user_index')


#reviewer views
def reviewerIndex(request):
    return render(request, 'reviewer/index.html')

