from django.urls import path, include
from . import views as user_views
from . import ViewEditor as editor_view
from django.contrib.auth import views as auth_views

urlpatterns = [
   path('login/', user_views.loginPage, name="login"),
   path('', user_views.homePage, name="home"),
   path('register', user_views.register, name="register"),
   path('logout/', user_views.logout_out, name="logout"),
   path('about/', user_views.about, name="about"),
   path('editor/', user_views.editor, name="editor"),
   path('articleSubmit/', user_views.articleSubmit, name="articleSubmit"),
   path('huuliPage/', user_views.huuliPage, name="huuliPage"),
   path('index_Article/', user_views.submit_index, name="index_Article"),
   path('article_send/', user_views.article_send, name="article_send"),
   path('article_list/', user_views.article_list, name="article_list"),
#reviewer
   path('index', user_views.reviewerIndex, name="index"),

   # admin urls
   path('admindashboard/', user_views.adminDashboard, name="admindashboard"),
   path('manage_user_index/', user_views.manage_user_index, name="manage_user_index"),
   path('add_user/', user_views.add_user, name="add_user"),
   path('create_user/', user_views.create_user, name="create_user"),
   path('edit_user/<str:user_id>/', user_views.edit_user, name="edit_user"),
   path('aeuser/<int:pk>', user_views.edit_user, name='aeuser'),


   # editor
      path('editordashboard/', editor_view.EditorDashboard, name="editordashboard"),
    path('manager_author/', editor_view.manager_author, name="manager_author"),
    path('editor_profile/', editor_view.editor_profile, name="editor_profile"),
    path('editor_profile_update/', editor_view.editor_profile_update, name="editor_profile_update"),
    path('send_to/', editor_view.send_to_review, name="send_to"),
    path('add_author_save/', editor_view.add_author_save, name="add_author_save"),
    path('add_user/', editor_view.add_user, name="add_user"),
    path('manager_use/', editor_view.manager_user, name="manage_user"),
    path('manager_review/', editor_view.manager_review, name="manager_review"),
    path('manager_review_add/', editor_view.manager_review_add, name="manager_review_add"),
    path('manage_article/', editor_view.ABookListView.as_view(), name="manage_article"),

    
]